package com.example.admin.turnworld;

import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.widget.TextView;
//------------//
//IMPORTANT: Works ONLY on Nexus 6 API 24 and if not on 25 (for reason read important note from line 105).
//------------//

/**
 * Creator: cfeier
 */
public class Level1 extends Activity implements SensorEventListener{
    private boolean checkpointReached = false;
    private boolean isOnLadder = false;
    private Sensor sensor;
    private SensorManager sensorManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level1);

        sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_GAME);

        Button home = (Button) findViewById(R.id.home_button);
        home.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(), WelcomeScreen.class);
                startActivityForResult(myIntent, 0);
            }

        });
    }


    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if(!isOnLadder){
            if(sensorEvent.values[0] >= 3 && sensorEvent.values[2] <= 7){
                turnController("normal");
                ImageView image = (ImageView)findViewById(R.id.character);
                ObjectAnimator anim = ObjectAnimator.ofFloat(image,"y",image.getY() + 10);
                anim.setDuration(0);
                anim.start();
                check("down");
            }else{
                turnController("turned");
                ImageView image = (ImageView)findViewById(R.id.character);
                ObjectAnimator anim = ObjectAnimator.ofFloat(image,"y",image.getY() - 10);
                anim.setDuration(0);
                anim.start();
                check("up");
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void turnController(String state) {
        if(Settings.switchControllers){
            if(state.equals("normal")){
                Button home = (Button)findViewById(R.id.home_button);
                home.setRotation(0);
                Button up = (Button)findViewById(R.id.button_up);
                up.setRotation(0);
                Button down = (Button)findViewById(R.id.button_down);
                down.setRotation(0);
                Button left = (Button)findViewById(R.id.button_left);
                left.setRotation(0);
                Button right = (Button)findViewById(R.id.button_right);
                right.setRotation(0);
                Button jump = (Button)findViewById(R.id.button);
                jump.setRotation(0);
                TextView text = (TextView)findViewById(R.id.textView);
                text.setRotation(0);
            }else{
                Button home = (Button)findViewById(R.id.home_button);
                home.setRotation(180);
                Button up = (Button)findViewById(R.id.button_up);
                up.setRotation(180);
                Button down = (Button)findViewById(R.id.button_down);
                down.setRotation(180);
                Button left = (Button)findViewById(R.id.button_left);
                left.setRotation(180);
                Button right = (Button)findViewById(R.id.button_right);
                right.setRotation(180);
                Button jump = (Button)findViewById(R.id.button);
                jump.setRotation(180);
                TextView text = (TextView)findViewById(R.id.textView);
                text.setRotation(180);
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    public void jump(View view){
        ImageView image = (ImageView)findViewById(R.id.character);
        ObjectAnimator anim = ObjectAnimator.ofFloat(image,"y",image.getY() - 75);
        anim.setDuration(0);
        anim.start();
        check("jump");
    }
    public void up(View view){
        if(isOnLadder){
            ImageView image = (ImageView)findViewById(R.id.character);
            ObjectAnimator anim = ObjectAnimator.ofFloat(image,"y",image.getY() - 50);
            anim.setDuration(0);
            anim.start();
            check("up");
        }
    }
    public void down(View view){
        if(isOnLadder){
            ImageView image = (ImageView)findViewById(R.id.character);
            ObjectAnimator anim = ObjectAnimator.ofFloat(image,"y",image.getY() + 50);
            anim.setDuration(0);
            anim.start();
            check("down");
        }
    }
    public void left(View view){
        ImageView image = (ImageView)findViewById(R.id.character);
        ObjectAnimator anim = ObjectAnimator.ofFloat(image,"x",image.getX() - 50);
        anim.setDuration(0);
        anim.start();
        check("left");
    }
    public void right(View view){
        ImageView image = (ImageView)findViewById(R.id.character);
        ObjectAnimator anim = ObjectAnimator.ofFloat(image,"x",image.getX() + 50);
        anim.setDuration(0);
        anim.start();
        check("right");
    }

    // READ, IMPORTANT NOTE!!!! Sorry, hatte nicht genug Zeit um eine bessere Lösung zu finden,
    // habe mich jetzt (6.9.2017 um 22:50) für diese komplett hässliche Lösung entschieden
    // und werde dafür die ganze Nacht brauchen, kann aber keine Fehler von der Zeit her leisten...
    // Hoffe auf wenig oder keinen Abzug, da es sowieso mehr um den Sensor und die Funktionalitäten geht.
    private void check(String method) { //returns true if hits something (only for 'gravity' needed)
        ImageView image = (ImageView) findViewById(R.id.character);
        float y = image.getY();
        float x = image.getX();
        isOnLadder = false;
        if (y <= 50) {
            respawn(image);
        }
        if(x >= 2100 && y <= 850 && y >= 800){
            showFinishScreen();
        }
        if (x <= 55) {
            moveImageTo("x", 55, image);
            if (y >= 910) {
                moveImageTo("y", 911, image);
            }
        } else if (x >= 55 && x <= 373) {          //part 1 start
            if (y >= 910) {
                moveImageTo("y", 911, image);
            }
        } else if (x >= 373 && x <= 430) {
            if (y >= 910) {
                moveImageTo("y", 911, image);
            }
            if (y >= 790) {
                isOnLadder = true;
            }
        } else if (x >= 410 && y >= 820 && x <= 459) {
            if (method.equals("down")) {
                moveImageTo("y", 811, image);
            } else {
                moveImageTo("x", 409, image);
            }
            if (y >= 910) {
                moveImageTo("y", 911, image);
            }                                   //part 1 end

        } else if (x >= 459 && x <= 700 && y >= 600) {         //part 2 start
            if (y >= 810) {
                moveImageTo("y", 811, image);
            }
        } else if (x >= 630 && x <= 710 && y >= 600) {
            if (y >= 810) {
                moveImageTo("y", 811, image);
            }
            if (y >= 690) {
                isOnLadder = true;
            }
        } else if (x >= 710 && y >= 730 && x <= 759) {
            if (method.equals("down")) {
                moveImageTo("y", 721, image);
            } else {
                moveImageTo("x", 709, image);
            }
            if (y >= 810) {
                moveImageTo("y", 811, image);
            }                                   //part 2 end
        } else if (x >= 759 && x <= 909 && y >= 650) {         //part 3 start
            if (y >= 720) {
                moveImageTo("y", 721, image);
            }                                   //part 3 end
        } else if (x >= 920 && y >= 650 && x <= 1200) {    //spikes start
            moveImageTo("y", 750, image);
            respawn(image);                     //spikes end

        } else if (x >= 650 && x <= 850 && y <= 300) {               //part 4 start (upside down part 1)
            if (y <= 110) {
                moveImageTo("y", 111, image);
            }
        } else if (x >= 840 && x <= 920) {
            if (y <= 110) {
                moveImageTo("y", 111, image);
            }
            if (y <= 300) {
                isOnLadder = true;
            }
        } else if (x >= 920 && y <= 280 && x <= 969) {
            if (method.equals("up")) {
                moveImageTo("y", 271, image);
            } else {
                moveImageTo("x", 919, image);
            }
            if (y <= 110) {
                moveImageTo("y", 111, image);
            }                                   //part 4 end (upside down part 1)
        } else if (x >= 969 && x <= 1500 && y <= 320) {         //part 5 start (upside down part 2)
            if (y <= 270) {
                moveImageTo("y", 271, image);
            }                                   //part 5 end (upside down part 2)
        } else if (x >= 1200 && x <= 1369 && y >= 725) {         //part 6 start
            if (y >= 725) {
                moveImageTo("y", 726, image);
            }                                   //part 6 end
        } else if (x >= 1369 && x <= 1519 && y >= 825) {         //part 7 start
            checkpointReached = true;
            if (y >= 825) {
                moveImageTo("y", 826, image);
            }                                   //part 7 end
        } else if (x >= 1519 && x <= 1719 && y >= 500) {         //part 8 start
            checkpointReached = true;
            if (y >= 1250) {
                moveImageTo("y", 1251, image);
            }
        } else if (x >= 1669 && x <= 1729 && y >= 500) {
            if (y >= 1250) {
                moveImageTo("y", 1251, image);
            }
            if (y >= 790) {
                isOnLadder = true;
            }
        } else if (x >= 1729 && y >= 830 && x <= 1778) {
            if (method.equals("down")) {
                moveImageTo("y", 821, image);
            } else {
                moveImageTo("x", 1728, image);
            }
            if (y >= 1250) {
                moveImageTo("y", 1251, image);
            }                                   //part 8 end
        } else if (x >= 1778 && x <= 2500 && y >= 610) {         //part 9 start
            checkpointReached = true;
            if (y >= 820) {
                moveImageTo("y", 821, image);
            }                                   //part 9 end

        }
    }

    private void showFinishScreen() {
        ImageView image = (ImageView) findViewById(R.id.win);
        image.setVisibility(View.VISIBLE);
    }


    private void respawn(ImageView image) {
        if(!checkpointReached){
            moveImageTo("x", 159, image);
            moveImageTo("y", 911, image);
        }else{
            moveImageTo("x", 1419, image);
            moveImageTo("y", 826, image);
        }
    }



    private void moveImageTo(String coordinate, int value, ImageView image) {
        ObjectAnimator anim = ObjectAnimator.ofFloat(image,coordinate,value);
        anim.setDuration(0);
        anim.start();
    }
}