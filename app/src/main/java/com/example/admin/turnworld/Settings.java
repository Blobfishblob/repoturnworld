package com.example.admin.turnworld;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import java.io.IOException;

/**
 * Creator: cfeier
 */
public class Settings extends Activity {
    public static boolean switchControllers = true;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        final CheckBox music = (CheckBox)findViewById(R.id.sound_checkbox);
        final MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.music);
        mediaPlayer.setLooping(true);
        music.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(music.isChecked()){
                    mediaPlayer.start();
                }else{
                    mediaPlayer.pause();
                }
            }
        });
        final CheckBox controller = (CheckBox)findViewById(R.id.switch_controller_checkbox);
        controller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(controller.isChecked()){
                    switchControllers = true;
                }else{
                    switchControllers = false;
                }
            }
        });
        Button back = (Button) findViewById(R.id.back_button);
        back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(), WelcomeScreen.class);
                startActivityForResult(myIntent, 0);
            }

        });
    }
}
